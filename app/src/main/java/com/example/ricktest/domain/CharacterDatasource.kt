package com.example.ricktest.domain


import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.ricktest.data.network.RemoteRickService
import com.example.ricktest.data.network.models.NetworkState
import com.example.ricktest.data.persistent.Location
import com.example.ricktest.data.persistent.LocationDatabase
import com.example.ricktest.presentation.LiveState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.Executor


class CharacterDatasource constructor(
    val dataProvider: RemoteRickService,
    private val retryExecutor: Executor,
    private val state: LiveState,
    private val locationDatabase: LocationDatabase
) :
    PageKeyedDataSource<Int, Character>() {
    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Character>) {
        // Ignored, because  we only use @loadAfter()
    }

    private var nextPage: Int? = null


    private var retry: (() -> Any)? = null


    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadAfter(
        params: PageKeyedDataSource.LoadParams<Int>,
        callback: PageKeyedDataSource.LoadCallback<Int, Character>
    ) {
        if (nextPage == null) return
        networkState.postValue(NetworkState.LOADING)
        compositeDisposable.add(
            getCharacterObservalable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({


                    callback.onResult(it, nextPage)
                    networkState.postValue(NetworkState.LOADED)

                }, {
                    retry = {
                        loadAfter(params, callback)
                    }
                    networkState.postValue(
                        NetworkState.error(" ${it.message}")
                    )
                })
        )

    }


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Character>
    ) {

        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)
        compositeDisposable.add(
            getCharacterObservalable()
                .subscribe({
                    retry = null
                    networkState.postValue(NetworkState.LOADED)
                    initialLoad.postValue(NetworkState.LOADED)
                    callback.onResult(it, null, nextPage)

                }, {
                    retry = {
                        loadInitial(params, callback)
                    }
                    val error = NetworkState.error(it.message ?: "unknown error")
                    networkState.postValue(error)
                    initialLoad.postValue(error)
                })
        )


    }

    private fun getCharacterObservalable() =
        dataProvider.requestCharacters(state.state, nextPage)
            .map {
                nextPage = it.pageInfo.nextPageNumber
                it.results
            }.toObservable()
            .flatMapIterable { it }.flatMap { character ->
                val locationId = character.location.id
                if (locationId == null) {
                    return@flatMap Observable.just(with(character) {
                        Character(
                            name,
                            status,
                            species,
                            gender,
                            imageUrl,
                            null
                        )
                    })
                } else
                    return@flatMap locationDatabase.locationDao().getLocationById(locationId).toObservable().map {
                        Timber.d("From db ${it.name}")
                        with(it) { com.example.ricktest.data.network.models.Location(id, name, type, dimension) }
                    }.switchIfEmpty(
                        dataProvider.requetsLocation(locationId).flatMapObservable { result ->
                            Timber.d("From internet ${result.name}")
                            locationDatabase.locationDao().insertLocation(
                                with(result) {
                                    Location(
                                        id,
                                        name,
                                        type,
                                        dimension
                                    )
                                })
                                .toObservable().map { result }
                        }).map {
                        with(character) {
                            Character(
                                name,
                                status,
                                species,
                                gender,
                                imageUrl,
                                it
                            )
                        }
                    }
            }
            .toList()


    fun dispose() {
        compositeDisposable.dispose()
    }


}