package com.example.ricktest.domain

import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import com.example.ricktest.data.network.RemoteRickService
import com.example.ricktest.data.persistent.LocationDatabase
import com.example.ricktest.presentation.LiveState
import java.util.concurrent.Executor

class Repository(
    private val rickService: RemoteRickService,
    private val networkExecutor: Executor,
    private val locationDatabase: LocationDatabase
) {
    fun getCharacters(pageSize: Int, state: LiveState): Listing<Character> {
        val sourceFactory = DataSourceFactory(rickService, networkExecutor, state,locationDatabase)

        val livePagedList = LivePagedListBuilder(sourceFactory, pageSize)
            .setFetchExecutor(networkExecutor)
            .build()

        return Listing(
            pagedList = livePagedList,
            networkState = Transformations.switchMap(sourceFactory.sourceLiveData, {
                it.networkState
            }),
            retry = {
                sourceFactory.sourceLiveData.value?.retryAllFailed()
            },
            refresh = {
                sourceFactory.sourceLiveData.value?.invalidate()
            },
            refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                it.initialLoad
            }
        )
    }
}