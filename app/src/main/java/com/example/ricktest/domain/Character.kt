package com.example.ricktest.domain

import com.example.ricktest.data.network.models.Location

data class Character (
    val name: String,
    val status: String,
    val species: String,
    val gender: String,
    val imageUrl:String,
    val location: Location?
    )

