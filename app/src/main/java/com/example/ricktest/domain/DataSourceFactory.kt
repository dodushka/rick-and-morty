package com.example.ricktest.domain

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.ricktest.data.network.RemoteRickService
import com.example.ricktest.data.persistent.LocationDatabase
import com.example.ricktest.presentation.LiveState
import java.util.concurrent.Executor

class DataSourceFactory(
    private val api: RemoteRickService,
    private val retryExecutor: Executor,
    private val state: LiveState,
    private val locationDatabase: LocationDatabase
) :
    DataSource.Factory<Int, Character>() {
    val sourceLiveData = MutableLiveData<CharacterDatasource>()
    override fun create(): DataSource<Int, Character> {
        val source = CharacterDatasource(api, retryExecutor, state,locationDatabase)
        sourceLiveData.postValue(source)
        return source
    }
}