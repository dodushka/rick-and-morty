package com.example.ricktest.di

import com.example.ricktest.presentation.CharacterListFragment
import com.example.ricktest.presentation.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(modules = arrayOf(AppModule::class, RemoteModule::class, DataSourceModule::class))
@Singleton
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(characterListFragment: CharacterListFragment)
}