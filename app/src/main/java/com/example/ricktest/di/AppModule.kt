package com.example.ricktest.di

import android.app.Application
import android.content.Context
import com.example.ricktest.app.App
import com.example.ricktest.domain.Repository
import com.pascal.openaq.city.ui.CharacterViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val application: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideViewModelFactory(app: Application, repository: Repository): CharacterViewModelFactory {
        return CharacterViewModelFactory(app, repository)
    }

    @Provides
    @Singleton
    fun provideApplication(): Application = application

}
