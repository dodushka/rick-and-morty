package com.example.ricktest.di


import android.content.Context
import com.example.ricktest.data.network.RemoteRickService
import com.example.ricktest.data.persistent.LocationDatabase
import com.example.ricktest.domain.Repository
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class DataSourceModule {
    @Provides
    @Singleton
    fun provideRepository(remoteRickService: RemoteRickService, context: Context) = Repository(
        remoteRickService,
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()),
        LocationDatabase.getInstance(context)
    )


}
