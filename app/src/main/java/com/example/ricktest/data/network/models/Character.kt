package com.example.ricktest.data.network.models

import com.google.gson.annotations.SerializedName

data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val gender: String,
    @SerializedName("image") val imageUrl: String,
    val location: LocationUrlHolder
)

data class LocationUrlHolder(val url: String) {
    val id: Int?
        get() {
            return url.split("/").last()?.toIntOrNull()
        }

}
