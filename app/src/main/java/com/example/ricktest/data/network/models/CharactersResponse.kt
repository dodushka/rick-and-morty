package com.example.ricktest.data.network.models

import android.net.Uri
import com.google.gson.annotations.SerializedName

data class CharactersResponse(@SerializedName("info") val pageInfo: PageInfo, val results: List<Character>)

data class PageInfo(val count: Int, val pages: Int, val next: String) {
    val nextPageNumber: Int?
        get()  {
            val uri = Uri.parse(next)
            return uri.getQueryParameter("page")?.toIntOrNull()
        }

}