package com.example.ricktest.data.network

import com.example.ricktest.data.network.models.CharactersResponse
import com.example.ricktest.data.network.models.Location
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteRickService {

    @GET("character")
    fun requestCharacters(
        @Query("status") sortOrder: String?,
        @Query("page") pageNumber: Int?
    ): Single<CharactersResponse>

    @GET("location/{location_id}")
    fun requetsLocation(@Path(value = "location_id") locationId: Int): Single<Location>
}