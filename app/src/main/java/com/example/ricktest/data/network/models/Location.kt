package com.example.ricktest.data.network.models

data class Location(
    val id: Int,
    val name: String,
    val type: String,
    val dimension: String
)