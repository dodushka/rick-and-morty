package com.example.ricktest.data.persistent

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface LocationDao {

    /**
     * Get a location by id.
     * @return the user from the table with a specific id.
     */
    @Query("SELECT * FROM Location WHERE id = :id")
    fun getLocationById(id: Int): Maybe<Location>

    /**
     * Insert a user in the database. If the user already exists, replace it.
     * @param location the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocation(location: Location): Single<Long>

    /**
     * Delete all users.
     */
    @Query("DELETE FROM Location")
    fun deleteAllULocations()
}