package com.example.ricktest.app

import android.app.Application
import com.example.ricktest.BuildConfig
import com.example.ricktest.di.AppComponent
import com.example.ricktest.di.AppModule
import com.example.ricktest.di.DaggerAppComponent
import com.example.ricktest.di.RemoteModule
import timber.log.Timber.DebugTree
import timber.log.Timber




class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }

    fun initializeDagger() {
       appComponent = DaggerAppComponent.builder()
          .appModule(AppModule(this))
            .remoteModule(RemoteModule()).build()
    }
}