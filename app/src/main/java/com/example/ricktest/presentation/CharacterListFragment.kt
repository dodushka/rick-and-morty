package com.example.ricktest.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ricktest.R
import com.example.ricktest.app.App
import com.pascal.openaq.city.ui.CharacterPagedListAdapter
import com.pascal.openaq.city.ui.CharacterViewModel
import com.pascal.openaq.city.ui.CharacterViewModelFactory
import kotlinx.android.synthetic.main.fragment_list.view.*
import javax.inject.Inject

class CharacterListFragment : Fragment() {
    private var listener: OnRetryListener? = null
    lateinit var adapter: CharacterPagedListAdapter
    @Inject
    lateinit var viewModelFactory: CharacterViewModelFactory
    private lateinit var viewModel: CharacterViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        App.appComponent.inject(this)
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        viewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(CharacterViewModel::class.java)
        adapter = CharacterPagedListAdapter { onRetry() }
        view.recyclerView.layoutManager = LinearLayoutManager(view.context)
        view.recyclerView.addItemDecoration(
            MarginItemDecoration(
                resources.getDimension(R.dimen.small_margin).toInt()
            )
        )
        view.recyclerView.adapter = adapter
        viewModel.cities.observe(
            this, Observer(
                adapter::submitList
            )
        )
        viewModel.networkState.observe(this, Observer {
            adapter.setNetworkState(
                it
            )
        })


        return view
    }


    fun onRetry() {
        listener?.onRetry()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnRetryListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnRetryListener {
        fun onRetry()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            CharacterListFragment()
    }
}