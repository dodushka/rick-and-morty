package com.example.ricktest.presentation

enum class LiveState(val state: String?) {
    ALIVE("alive"), DEAD("dead"), ALL(null)

}