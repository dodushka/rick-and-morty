package com.pascal.openaq.city.ui


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ricktest.R
import com.example.ricktest.data.network.models.NetworkState
import com.example.ricktest.domain.Character
import com.github.florent37.glidepalette.BitmapPalette
import com.github.florent37.glidepalette.GlidePalette
import kotlinx.android.synthetic.main.character_item.view.*

class CharacterPagedListAdapter(private val retryCallback: () -> Unit) :
    PagedListAdapter<Character, RecyclerView.ViewHolder>(diffCallback) {

    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.character_item -> (holder as CharacterViewHolder).bindTo(getItem(position))
            R.layout.network_state_item -> (holder as NetworkStateItemViewHolder).bindTo(
                networkState
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.character_item -> CharacterViewHolder.create(parent)
            R.layout.network_state_item -> NetworkStateItemViewHolder.create(parent, retryCallback)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }


    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.network_state_item
        } else {
            R.layout.character_item
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    class CharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindTo(character: Character?) {
            character?.let {
                with(it) {
                    Glide.with(itemView).load(imageUrl).listener(
                        GlidePalette.with(imageUrl).use(BitmapPalette.Profile.MUTED_LIGHT)
                            .intoBackground(itemView.location_background, BitmapPalette.Swatch.RGB)

                            .intoTextColor(itemView.location_name, BitmapPalette.Swatch.BODY_TEXT_COLOR)
                            .intoTextColor(itemView.location_name_value, BitmapPalette.Swatch.BODY_TEXT_COLOR)
                            .intoTextColor(itemView.location_type, BitmapPalette.Swatch.BODY_TEXT_COLOR)
                            .intoTextColor(itemView.location_type_value, BitmapPalette.Swatch.BODY_TEXT_COLOR)
                            .intoTextColor(itemView.location_dimension, BitmapPalette.Swatch.BODY_TEXT_COLOR)
                            .intoTextColor(itemView.location_dimension_value, BitmapPalette.Swatch.BODY_TEXT_COLOR)
                            .crossfade(true)
                    ).into(itemView.icon)
                    itemView.name_value.text = name
                    itemView.gender_value.text = gender
                    itemView.species_value.text = species
                    itemView.status_value.text = status
                    itemView.location_name_value.text = location?.name ?: itemView.context.getString(R.string.unknown)
                    itemView.location_type_value.text = location?.type ?: itemView.context.getString(R.string.unknown)
                    itemView.location_dimension_value.text =
                        location?.dimension ?: itemView.context.getString(R.string.unknown)
                }
            }

        }

        companion object {
            fun create(parent: ViewGroup): CharacterViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.character_item, parent, false)
                return CharacterViewHolder(view)
            }
        }

    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Character>() {
            override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean = oldItem.name == newItem.name
            override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean = oldItem == newItem
        }
    }
}
