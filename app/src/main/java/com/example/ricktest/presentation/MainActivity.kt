package com.example.ricktest.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.ricktest.R
import com.example.ricktest.app.App
import com.example.ricktest.presentation.CharacterListFragment.OnRetryListener
import com.pascal.openaq.city.ui.CharacterViewModel
import com.pascal.openaq.city.ui.CharacterViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), NavFragment.OnLiveStateSelectedListener, OnRetryListener {


    private val TAG_NAV_FRAGMENT: String = "TAG_NAV_FRAGMENT"
    private val TAG_LIST_FRAGMENT: String = "TAG_LIST_FRAGMENT"
    @Inject
    lateinit var viewModelFactory: CharacterViewModelFactory
    private lateinit var viewModel: CharacterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.nv, NavFragment.newInstance(), TAG_NAV_FRAGMENT).commit()
            supportFragmentManager.beginTransaction()
                .add(R.id.container, CharacterListFragment.newInstance(), TAG_LIST_FRAGMENT).commit()
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CharacterViewModel::class.java)
        initQuery(null)
    }

    private fun initQuery(liveState: LiveState?) {
        viewModel.liveState = liveState
        viewModel.startLoading()

    }

    override fun onRetry() {
        viewModel.retry()
    }

    override fun onLiveStateSelected(liveState: LiveState) {
        drawer_layout.closeDrawers()
        initQuery(liveState)
    }
}
