package com.pascal.openaq.city.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

import com.example.ricktest.domain.Character
import com.example.ricktest.domain.Listing
import com.example.ricktest.domain.Repository
import com.example.ricktest.presentation.LiveState
import javax.inject.Inject


class CharacterViewModel @Inject constructor(app: Application, private val repository: Repository) :
    AndroidViewModel(app) {


    var liveState: LiveState? = null
    private val repoResult = MutableLiveData<Listing<Character>>()
    val cities = Transformations.switchMap(repoResult, { it.pagedList })!!
    val networkState = Transformations.switchMap(repoResult, { it.networkState })!!


    fun startLoading() {
        repoResult.postValue(repository.getCharacters(PAGED_LIST_PAGE_SIZE, liveState?:LiveState.ALL))
    }

    fun retry() {
        val listing = repoResult?.value
        listing?.retry?.invoke()
    }


    companion object {
        private const val PAGED_LIST_PAGE_SIZE = 20
    }
}