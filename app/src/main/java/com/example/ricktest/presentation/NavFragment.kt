package com.example.ricktest.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.example.ricktest.R
import kotlinx.android.synthetic.main.fragment_nav_drawer.view.*


class NavFragment : Fragment() {
    private var listener: OnLiveStateSelectedListener? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_nav_drawer, container, false)

        val adapter = ArrayAdapter<String>(
            view.context,
            android.R.layout.simple_spinner_item,
            mutableListOf("All", "Dead", "Alive")
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        view.spinner.adapter = adapter
        view.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    1 -> onItemSelected(LiveState.DEAD)
                    2 -> onItemSelected(LiveState.ALIVE)
                    else -> onItemSelected(LiveState.ALL)
                }
            }

        }

        return view
    }


    fun onItemSelected(liveState: LiveState) {
        listener?.onLiveStateSelected(liveState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnLiveStateSelectedListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnLiveStateSelectedListener {
        fun onLiveStateSelected(liveState: LiveState)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            NavFragment().apply {}
    }
}
